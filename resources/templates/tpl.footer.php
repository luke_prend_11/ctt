        <br class="clearfloat" />
        </div>
    </div>

    <footer>
        <div class="content">
            <div class="stretch-parent">
                <ul class="stretch-child">
                    <li>
                        <strong><a href="<?php echo config::$baseUrl; ?>" title="Home Page">Home</a></strong>
                    </li>
                    <li>
                        <a href="<?php echo config::$baseUrl; ?>/help/about-us.php" title="About Us">About Us</a>
                    </li>
                    <li>
                        <a href="<?php echo config::$baseUrl; ?>/help/privacy.php" title="Privacy Policy">Privacy Policy</a>
                    </li>
                    <li>
                        <a href="<?php echo config::$baseUrl; ?>/site-map.php" title="Site Map">Site Map</a>
                    </li>
                </ul>
                <ul class="stretch-child">
                    <li>
                        <strong><a href="<?php echo config::$baseUrl; ?>/services/" title="Our Services">Services</a></strong>
                    </li>
                    <li>
                        <a href="<?php echo config::$baseUrl; ?>/services/close-protection.php" title="Close Protection">Close Protection</a>
                    </li>
                    <li>
                        <a href="<?php echo config::$baseUrl; ?>/services/retail-security.php" title="Retail Security">Retail Security</a>
                    </li>
                    <li>
                        <a href="<?php echo config::$baseUrl; ?>/services/manned-guarding.php" title="Manned Guarding">Manned Guarding</a>
                    </li>
                    <li>
                        <a href="<?php echo config::$baseUrl; ?>/services/security-guards.php" title="Provision of Security Guards">Provision of Security Guards</a>
                    </li>
                </ul>
                <ul class="stretch-child">
                    <li>
                        <strong><a href="<?php echo config::$baseUrl; ?>/training/" title="Training">Training</a></strong>
                    </li>
                    <li>
                        <a href="<?php echo config::$baseUrl; ?>/training/close-protection.php" title="Close Protection">Close Protection</a>
                    </li>
                    <li>
                        <a href="<?php echo config::$baseUrl; ?>/training/door-supervision.php" title="Door Supervision">Door Supervision</a>
                    </li>
                    <li>
                        <a href="<?php echo config::$baseUrl; ?>/training/cctv-public-space-surveillance.php" title="CCTV Surveillance">CCTV Surveillance</a>
                    </li>
                    <li>
                        <a href="<?php echo config::$baseUrl; ?>/training/conflict-management.php" title="Conflict Management">Conflict Management</a>
                    </li>
                    <li>
                        <a href="<?php echo config::$baseUrl; ?>/training/physical-intervention.php" title="Physical Intervention">Physical Intervention</a>
                    </li>
                    <li>
                        <a href="<?php echo config::$baseUrl; ?>/training/first-aid.php" title="First Aid">First Aid</a>
                    </li>
                </ul>
                <ul class="stretch-child">
                    <li>
                        <strong><a href="<?php echo config::$baseUrl; ?>/help/contact-us.php" title="Contact Us">Contact Us</a></strong>
                    </li>
                    <li>
                        <span class="phone">
                            <a href="tel:<?php echo config::CONTACT_PHONE_1; ?>" title="Call <?php echo config::SITE_NAME; ?>">
                                <?php echo config::CONTACT_PHONE_1; ?>
                            </a>
                        </span>
                    </li>
                    <li>
                        <span class="email">
                            <a href="mailto:<?php echo config::CONTACT_EMAIL; ?>" title="Email <?php echo config::SITE_NAME; ?>">
                                <?php echo config::CONTACT_EMAIL; ?>
                            </a>
                        </span>
                    </li>
                    <br />
                    <li>
                        <a href="<?php echo config::FACEBOOK; ?>" title="<?php echo config::SITE_NAME; ?> on Facebook" class="fb"></a>
                        <a href="<?php echo config::LINKED_IN; ?>" title="<?php echo config::SITE_NAME; ?> on Linked In" class="in"></a>
                    </li>
                </ul>
                <span class="stretch"></span>
            </div>

            <div id="copyright">
                <span>
                    <?php echo config::DOMAIN; ?> &copy; <?php echo date('Y'); ?>. All Rights Reserved | 
                    Website Designed &amp; Developed by <a href="https://lpwebdesign.co.uk" title="LP Web Design" target="_blank">LP Web Design</a>
                </span>
            </div>
        </div>
    </footer>
	
    <!-- smooth scroll anchor links -->
    <script type="text/javascript">
        var jsvar = window.location.hash;

        if (jsvar) {
            $(document).ready(function(){
                event.preventDefault();
                $('html, body').animate({
                        scrollTop: $(jsvar).offset().top
                }, 1000);
                return false;
            });
        }
    </script>

    <!-- responsive navigation menu -->
    <script>
        var nav = responsiveNav(".nav-collapse");
    </script>

    <!-- show/hide function -->
    <script>
    $(document).ready(function(){
        $("h3.toggle").click(function(){
            $("div.toggle").toggle();
        });
    });
    </script>
</body>
</html>
