<?php
class notifications
{
	public static $scoringPointsInfo;
	
	public static function showError($myError, $openingMessage = TRUE)
	{
		$msg = "<p class='error'>";
		$msg .= $openinMessage = TRUE ? "<strong>Please correct the following error" : "";
		
		$num_errors = count($myError);
		if($num_errors > 1) {
			$msg .= "s";
		}
		
		$msg .= $openinMessage = TRUE ? ":</strong><br />" : "";
			
		if(is_array($myError)) {	
			foreach ($myError as $error) {
				$msg .= $error."<br />";
			}
		}
		else {
			$msg .= $myError;
		}
		
		$msg .= "</p>";
		echo $msg;
	}
	
	public static function showSuccess($mySuccess)
	{
		$msg = "<p class='success'><strong>Success:</strong><br />";
		$msg .= $mySuccess;
		$msg .= "</p>";
		echo $msg;
	}
}
?>