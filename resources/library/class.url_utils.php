<?php
class url_utils
{
	public function curPageURL() {
		$pageURL = 'http';
		if (isset($_SERVER["HTTPS"]) && strtolower($_SERVER["HTTPS"]) == "on") {
			$pageURL .= "s";
		}
		$pageURL .= "://";
		if ($_SERVER["SERVER_PORT"] != "80") {
			$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		} else {
			$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		}
		return $pageURL;
	}
	
	public function parse_query_string($query_string) {
		// split the query string into individual name-value pairs
		$items = explode('&', $query_string);
		// initialize the return array
		$qs_array = array();
		// create the array
		foreach($items as $i) {
			// split the name-value pair and save the elements to $qs_array
			$pair = explode('=', $i);
			$qs_array[urldecode($pair[0])] = urldecode($pair[1]);
		}
		// return the array
		return $qs_array;
	}
	
	public function remove_query_param($url, $param) {
		// extract the query string from $url
		$tokens = explode('?', $url);
		$url_path = $tokens[0];
		$query_string = $tokens[1];
		// transform the query string into an associative array
		$qs_array = $this->parse_query_string($query_string);
		// remove the $param element from the array
		unset($qs_array[$param]);
		// create the new query string by joining the remaining parameters
		$new_query_string = '';
		if ($qs_array) {
			foreach ($qs_array as $name => $value) {
				$new_query_string .= ($new_query_string == '' ? '?' : '&').urlencode($name).'='.urlencode($value);
			}
		}
		// return the URL that doesn’t contain $param
		$full_path = $url_path . $new_query_string;
		$full_path = str_replace("index.php","",$full_path);
		return $full_path;
	}
}
?>