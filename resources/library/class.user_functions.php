<?php
class user_functions
{
	public function valid_email($email)
	{
		// First, we check that there's one @ symbol, and that the lengths are right
		if (!preg_match("/^[^@]{1,64}@[^@]{1,255}$/", $email)) {
			// Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
			return false;
		}
		// Split it into sections to make life easier
		$email_array = explode("@", $email);
		$local_array = explode(".", $email_array[0]);
		for ($i = 0; $i < sizeof($local_array); $i++) {
			if (!preg_match("/^(([A-Za-z0-9!#$%&#038;'*+=?^_`{|}~-][A-Za-z0-9!#$%&#038;'*+=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$/",
				$local_array[$i])) {
				return false;
			}
		}
		if (!preg_match("/^\[?[0-9\.]+\]?$/", $email_array[1])) { // Check if domain is IP. If not, it should be valid domain name
			$domain_array = explode(".", $email_array[1]);
			if (sizeof($domain_array) < 2) {
				return false; // Not enough parts to domain
			}
			for ($i = 0; $i < sizeof($domain_array); $i++) {
				if (!preg_match("/^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$/", $domain_array[$i])) {
					return false;
				}
			}
		}
		return true;
	}
	
	public function showStickyForm($type, $name, $values = '', $autocomplete = false, $label = '')
	{
		if($autocomplete == true) {
			$auto = " autocomplete='off'";
		}
		else {
			$auto = "";
		}
		
		$item_name = ' name="'.$name.'"';
		
		$string = '<div>';
		$string .= '<label for="'.$name.'">';
		$string .= $label <> '' ? $label : ucwords(str_replace("_"," ",$name));
		$string .= '</label>';
			
		if($type == 'text') {
			$string .= '<input type="text"'.$item_name.' value="';
			if(isset($_POST[$name])) {
				$string .= $_POST[$name];
			}
			elseif($name == 'referred_by' && isset($_SESSION['referral'])) {
				$string .= (htmlspecialchars($_SESSION['referral'], ENT_QUOTES));
			}
			$string .= '"'.$auto.'>'; 	
		}
		elseif($type == 'password') {
			$string .= '<input type="password"'.$item_name.' value="';
			if(isset($_POST[$name])) {
				$string .= $_POST[$name];
			}
			$string .= '"'.$auto.' maxlength="15">'; 	
		}
		elseif($type == 'select') {
			$string .= '<select'.$item_name.'">';
			$string .= '<option></option>';
			foreach($values as $x) {
			$string .= '<option';
				if(isset($_POST[$name]) && $_POST[$name] == $x) {
					$string .= ' selected="selected"';
				}
				$string .= '>'.$x.'</option>';
			}
		  	$string .= '</select>';
		}
		elseif($type == 'textarea') {
			$string .= '<textarea'.$item_name.'>';
			if(isset($_POST[$name])) {
				$string .= htmlentities($_POST['comments'], ENT_QUOTES);
			}
			$string .= '</textarea>';	
		}
		else {
			 $msg = new msgs();
			 $msg->showError("Input type appears to be invalid");
		}
		
		$string .= '</div>';
		return $string;
	}
	
	public function contactForm()
	{
		echo '
		<div id="form" class="float-left">
			<p>If you have any questions about our services or training courses or you are requesting to hire our Security Services, please use the form below to contact us.</p>
			
			<form name="contactform" method="post" action="'.htmlspecialchars($_SERVER['PHP_SELF']).'" class="form">
			<div>'
				.$this->showStickyForm('text','name')
				.$this->showStickyForm('text','email_address')
				.$this->showStickyForm('textarea','comments').
			'</div>
			<input type="submit" value="Submit" name="submitform" id="Submit Form" class="button" />
			</form>
		</div>';
	}
	
	public function checkContactForm($name, $email_from, $comments)
	{
		$errors = array();
		if(empty($name) || is_numeric($name)) {
			$errors[] = 'Enter a name';
		}
		if(!$this->valid_email($email_from)) {
			$errors[] = 'Enter a valid email address';
		}
		if(strlen($comments) < 4) {
			$errors[] = 'Enter your comments';
		}
		return $errors;
	}
	
	public function sendContactForm($name, $email_from, $email_to, $comments)
	{
		$sendMail = new mail();
		$email_subject = "Website Enquiry";
			 
		function clean_string($string) {
			$bad = array("content-type","bcc:","to:","cc:","href");
			return str_replace($bad,"",$string);
		}
		$email_message = "Form details below.\n\n";
		$email_message .= "Name: ".clean_string($name)."\n";
		$email_message .= "Email: ".clean_string($email_from)."\n";
		$email_message .= "Comments: ".clean_string($comments)."\n";
		
		if ($sendMail->sendMail($email_to, $email_subject, $email_message, $email_from)) {
			return true;
		}
		else {
			return false;
		}
	}
}
?>