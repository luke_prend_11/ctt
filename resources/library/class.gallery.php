<?php
class gallery
{	
	const DIRECTORY  = 'img/content/gallery/%s/';
	const CATEGORIES = array('Domestic', 'Commercial', 'Industrial', 'Education');
	
	private $currentCategory;
	private $currentCategoryDirectory;
	private $currentCategoryCount;
	
	private function buildGalleryListItem()
	{
		echo '
		<a name="'.strtolower($this->currentCategory).'" id="'.strtolower($this->currentCategory).'"></a>
		<h2>'.$this->currentCategory.'</h2>';
		
		if(!empty($this->currentCategoryCount)) {
			echo '
			<div id="gallery">
			<ul>';
			
			for ($i = 1; $i <= $this->currentCategoryCount; $i++) {
				echo '
				<li>
					<a href="'.config::$baseUrl.'/'.$this->currentCategoryDirectory.$this->currentCategory.$i.'.jpg" title="Caption goes here.">
						<div class="sq-thumb" style="background-image:url(\''.config::$baseUrl.'/'.$this->currentCategoryDirectory.$this->currentCategory.$i.'.jpg\');"></div>
					</a>
				</li>';
			}
			
			echo '
			</ul>
			<br class="clearfloat" />
			</div>';
			
		}
		else {
			echo 'There are currently no photos available for this category.';
		}
	}
	
	private function setCurrentCategory($cat)
	{
		$this->currentCategory = $cat;
	}
	
	private function setCurrentCategoryDirectory()
	{
		$this->currentCategoryDirectory = sprintf(self::DIRECTORY, strtolower($this->currentCategory));
	}
	
	private function setCurrentCategoryCount()
	{
		$files = $this->getFiles();
		
		if ($files !== false)
		{
			$this->currentCategoryCount = count($files);
		}
		else
		{
			$this->currentCategoryCount = 0;
		}
	}
	
	private function getFiles()
	{
		return glob($this->currentCategoryDirectory.'*.jpg');
	}
	
	public function getPhotos()
	{
		foreach(self::CATEGORIES as $cat)
		{
			$this->setCurrentCategory($cat);
			$this->setCurrentCategoryDirectory();
			$this->setCurrentCategoryCount();
			$this->buildGalleryListItem();
		}
	}
}
?>