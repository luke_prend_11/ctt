<?php
class config
{
	const DOMAIN                = "closetacticaltraining.com";
	const BASE_URL_LIVE         = "http://www.closetacticaltraining.com";
        const BASE_URL_TEST         = "http://localhost/ctt"; // http://localhost:8888/tff
        const BASE_URL_LIVE_TESTING = "http://uk.ezeeinternet.net/~vjrnrejl/test/ctt";
	const SITE_NAME             = "Close Tactical Training Ltd";
	const PAGE_TITLE_SEPARATOR  = " | ";
	const CONTACT_ADDRESS       = "102 Spencer Street, Birmingham, B18 6JZ";
	const CONTACT_PHONE_1       = "0121 507 0638";
	const CONTACT_PHONE_2       = "";
	const CONTACT_EMAIL         = "enquiries@closetacticaltraining.com";
	const FACEBOOK              = "https://www.facebook.com/closetacticaltraining";
	const LINKED_IN             = "http://www.linkedin.com/pub/james-ford/25/b4/23b";

	private $host;
	private $username;
	private $password;
	private $database;
	private $websiteCredentials;
	
	public static $mysqli;
        public static $baseUrl;
	
	public function __construct()
	{
		// start a session
		$this->sessionStart();
                
                // set base url
                $this->setBaseUrl();
		
		// set website credentials
		$this->setWebsiteCredentials();
		
		// set database credentials
		// $this->setDatabaseCredentials();
		
		// set connection to the database
		// $this->setDatabaseConnection();
	}

        private function setBaseUrl()
        {
            self::$baseUrl = strpos($_SERVER['HTTP_HOST'], 'localhost') !== FALSE ? self::BASE_URL_TEST 
                    : (strpos(self::BASE_URL_LIVE_TESTING, $_SERVER['SERVER_NAME']) 
                    ? self::BASE_URL_LIVE_TESTING : self::BASE_URL_LIVE);
        }
	
	private function setWebsiteCredentials()
	{
		$this->websiteCredentials = array(
			"db" => array(
				"dblive" => array(
					"dbname"   => "vjrnrejl_ctt",
					"username" => "vjrnrejl_luke",
					"password" => "Password1",
					"host"     => "localhost"
				),
				"dbtest" => array(
					"dbname"      => "ctt",
					"username"    => "root",
					"password"    => "",  // IB1333143435
					"passwordmac" => "root",
					"host"        => "localhost"
				)
			),
			"paths" => array(
				"templates" => "/resources/templates/",
				"images"    => array(
					"content" => self::$baseUrl.'/img/content',
					"layout"  => self::$baseUrl.'/img/layout'
				)
			)
		);
	}
	
	private function setDatabaseCredentials()
	{
		$this->host     = $this->websiteCredentials['db']['dblive']['host'];
		$this->username = $this->websiteCredentials['db']['dblive']['username'];
		$this->password = $this->websiteCredentials['db']['dblive']['password'];
		$this->database = $this->websiteCredentials['db']['dblive']['dbname'];
	}
	
	private function setDatabaseConnection()
	{
		if (!isset(self::$mysqli)) {
			self::$mysqli = new mysqli($this->host, $this->username, $this->password, $this->database);
		}
		// check for database connection
		if (self::$mysqli->connect_errno) {
			printf("Connect failed: %s\n", self::$mysqli->connect_error);
			exit();
		}
	}
	
	private function sessionStart()
	{
		// Check if session has already been started
		if (session_id() == '') {
			$session_name = 'sec_session_id'; // Set a custom session name
			$secure       = false; // Set to true if using https.
			$httponly     = true; // This stops javascript being able to access the session id. 
	 
			ini_set('session.use_only_cookies', 1); // Forces sessions to only use cookies. 
			$cookieParams = session_get_cookie_params(); // Gets current cookies params.
			session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly); 
			session_name($session_name); // Sets the session name to the one set above.
			session_start(); // Start the php session
			session_regenerate_id(true); // Regenerated the session, delete the old one.    
		}
	}
}
?>