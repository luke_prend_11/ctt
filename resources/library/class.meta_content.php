<?php
class meta_content
{	
	const DEFAULT_META_KEYWORDS    = "Close Tactical Training, Close Protection, Door Supervision, CCTV Public Surveillance, Conflict Management Training, First Aid Training";
	const DEFAULT_META_DESCRIPTION = "";
	const STYLESHEET_LINK          = '<link rel="stylesheet/less" type="text/css" href="%s/css/%s" />';
	
	public $currentPageRoute;
	
	public $pageTitle;
	public $metaDescription;
	public $metaKeywords;
	public $pageH1;
	public $pageH2;
	public $css;
	public $lightBox;
	
	private $metaContent = array();
	
	public function __construct()
	{
		$this->getCurrentPage();
		
		$this->setMetaContent();
		$this->setPageTitle();
		$this->setMetaDescription();
		$this->setMetaKeywords();
		$this->setPageH1();
		$this->setPageH2();
		$this->setCss();
		$this->setLightBox();
	}
	
	public function setMetaContent()
	{
		$this->metaContent = array(
			"index" => array(
				"page_title"       => "Close Protection Training &amp; Services",
				"meta_description" => config::SITE_NAME." offer the very best in Security, Health & Safety training, not only for the individual but for your entire work force and company.",
				"h1"               => "Close Tactical Training",
				"css"              => "index.less"
			),
			"error" => array(
				"page_title"       => "Page Not Found",
				"meta_description" => "The page you have requested has not been found.",
				"h1"               => "Page Not Found",
			),
			"site_map" => array(
				"page_title"       => "Site Map",
				"meta_description" => "Links to all pages on ".config::SITE_NAME,
				"h1"               => "Site Map",
			),
			"services_close_protection" => array(
				"page_title"       => "Close Protection Services",
				"meta_description" => config::SITE_NAME." have supplied Close Protection to clients throughout the UK, USA, Europe, Saudi Arabia and Africa.",
				"h1"               => "Close Protection",
			),
			"services_index" => array(
				"page_title"       => "Protection Services",
				"meta_description" => config::SITE_NAME." offer the the following services: Close Protection, Retail Security, Manned Guarding, Provision of Security Guards",
				"h1"               => "Our Services",
			),
			"services_manned_guarding" => array(
				"page_title"       => "Manned Guarding",
				"meta_description" => config::SITE_NAME." can provide a twenty first century version of the &ldquo;Night Watchman&rdquo; to patrol and protect your sites.",
				"h1"               => "Manned Guarding",
			),
			"services_retail_security" => array(
				"page_title"       => "Retail Security",
				"meta_description" => config::SITE_NAME." have been providing retail security solutions to businesses nationwide for many years.",
				"h1"               => "Retail Security",
			),
			"services_security_guards" => array(
				"page_title"       => "Provison of Security Guards",
				"meta_description" => "All our security guards and staff are fully trained, licensed in accordance with SIA (Security Industry Authority) Legislation and screened to BS7858 (5 year work or back to school checkable history & CRB Cleared).",
				"h1"               => "Provison of Security Guards",
			),
			"training_cctv_public_space_surveillance" => array(
				"page_title"       => "CCTV Public Space Surveillance Operations Level 2",
				"meta_description" => "CCTV Public Space Surveillance Operations Level 2 Training by Close Tactical Training Limited.",
				"h1"               => "CCTV Public Space Surveillance Operations Level 2",
			),
			"training_close_protection" => array(
				"page_title"       => "Close Protection Training Level 3",
				"meta_description" => "The Close Protection course is run by our highly qualified trainers who have over 100 years of CP experience combined.",
				"h1"               => "Close Protection Training Level 3",
			),
			"training_conflict_management" => array(
				"page_title"       => "Conflict Management Training",
				"meta_description" => config::SITE_NAME."'s Conflict Management Training course description.",
				"h1"               => "Conflict Management Training",
			),
			"training_door_supervision" => array(
				"page_title"       => "Level 2 SIA Door Supervisor Training Courses",
				"meta_description" => "The SIA Door Supervisor Training Qualification consists of 30 hours contact training time over 4 days which our trainers will be there to mentor you on.",
				"h1"               => "Level 2 SIA Door Supervisor Training Courses",
			),
			"training_first_aid" => array(
				"page_title"       => "First Aid at Work",
				"meta_description" => config::SITE_NAME." First Aid training courses.",
				"h1"               => "First Aid at Work",
			),
			"training_index" => array(
				"page_title"       => "Training Courses",
				"meta_description" => config::SITE_NAME." offer the very best in Security, Health & Safety training, not only for the individual but for your entire work force and company.",
				"h1"               => "Training Courses",
			),
			"training_physical_intervention" => array(
				"page_title"       => "Physical Intervention Training Level 2 and Level 3",
				"meta_description" => config::SITE_NAME."'s Physical Intervention Training for Level 2 and Level 3.",
				"h1"               => "Physical Intervention Training Level 2 and Level 3",
			),
			"help_about_us" => array(
				"page_title"       => "About Us",
				"meta_description" => "With years of experience our fully qualified staff are able to offer a professional close protection services and training.",
				"h1"               => "About Us",
			),
			"help_contact_us" => array(
				"page_title"       => "Contact Us",
				"meta_description" => "Contact ".config::SITE_NAME.".",
				"h1"               => "Contact Us",
				"css"              => "forms.less"
			),
			"help_index" => array(
				"page_title"       => "Help",
				"meta_description" => "With years of experience and fully qualified staff we are able to offer a professional and reliable close protection service.",
				"h1"               => "Help",
			),
			"help_privacy" => array(
				"page_title"       => "Privacy Policy",
				"meta_description" => "Find out more about our privacy policy.",
				"h1"               => "Privacy Policy",
			)
		);
	}
	
	private function getCurrentPage()
	{
		$to_replace = array("/",".php","ctt_test", "test_", "ctt","closetacticaltraining.com","~vjrnrejl_","-","__");
		$replace_with = array("_","","","","","","","_","_");
		
		$this->setCurrentPage(substr(str_replace($to_replace, $replace_with, ($_SERVER['PHP_SELF'])),1));
	}
	
	private function setCurrentPage($curPageRoute)
	{
		$this->currentPageRoute = $curPageRoute;
	}
	
	private function setPageTitle()
	{
		$this->pageTitle = (isset($this->metaContent[$this->currentPageRoute]["page_title"]) && $this->metaContent[$this->currentPageRoute]["page_title"] != ""
					     ? $this->metaContent[$this->currentPageRoute]["page_title"].config::PAGE_TITLE_SEPARATOR.config::SITE_NAME
					     : config::SITE_NAME);
	}
	
	private function setMetaDescription()
	{
		$this->metaDescription = (isset($this->metaContent[$this->currentPageRoute]["meta_description"]) && $this->metaContent[$this->currentPageRoute]["meta_description"] != ""
		                       ? $this->metaContent[$this->currentPageRoute]["meta_description"] 
							   : self::DEFAULT_META_DESCRIPTION);
	}
	
	private function setMetaKeywords()
	{
		$this->metaKeywords = (isset($this->metaContent[$this->currentPageRoute]["meta_keywords"]) && $this->metaContent[$this->currentPageRoute]["meta_keywords"] != ""
		                    ? $this->metaContent[$this->currentPageRoute]["meta_keywords"] 
							: self::DEFAULT_META_KEYWORDS);
	}
	
	private function setPageH1()
	{
		$this->pageH1 = isset($this->metaContent[$this->currentPageRoute]["h1"]) ? $this->metaContent[$this->currentPageRoute]["h1"] : "My Account";
	}
	
	private function setPageH2()
	{
		if(isset($this->metaContent[$this->currentPageRoute]["h2"])) {
			$this->pageH2 = $this->metaContent[$this->currentPageRoute]["h2"];
		}
	}
	
	private function setCss()
	{
		if(isset($this->metaContent[$this->currentPageRoute]["css"])) {
			$this->buildCss($this->metaContent[$this->currentPageRoute]["css"]);
		}
	}
	
	private function createCssLink($css)
	{
		return sprintf(self::STYLESHEET_LINK, config::$baseUrl, $css);
	}
	
	private function buildCss($css)
	{
		if(is_array($css)) {
			$this->css = "";
			foreach ($css as $stylesheet) {
				$this->css .= $this->createCssLink($stylesheet);
				$this->css .= "\n";
			}
		}
		else {
			$this->css = $this->createCssLink($css);
			$this->css .= "\n";
		}
	}
	
	private function setLightBox()
	{
		if(isset($this->metaContent[$this->currentPageRoute]["lightbox"]) && $this->metaContent[$this->currentPageRoute]["lightbox"] = 'yes') {
			$this->lightBox = TRUE;
		}
	}
}
?>