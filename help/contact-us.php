<?php
require_once '../resources/inc.config.php';
require_once '../resources/templates/tpl.header.php';
?>

<?php

$uf = new user_functions();
if (isset($_POST['submitform'])) {
	$errors = $uf->checkContactForm($_POST['name'], $_POST['email_address'], $_POST['comments']);
	if(empty($errors)) {
		if ($uf->sendContactForm($_POST['name'], $_POST['email_address'], config::CONTACT_EMAIL, $_POST['comments'])) {
			// display error if contact form fails to send
			notifications::showSuccess('Thank you for contacting '.config::SITE_NAME.'.<br />We aim to respond to your message within 24 hours.');
		}
		else {
			// display error if contact form fails to send
			notifications::showError('The contact form is currently not working please send an email to the email address directly if you\'d like to get in touch.');
			// display contact form
			$uf->contactForm();
		}
	}
	else {
		// display any errors that have occured
		notifications::showError($errors);
		// display contact form
		$uf->contactForm();
	}
}
else {
	$uf->contactForm();
}

?>
<div id="contact-info" class="float-right very-light-grey">
	<h2>Contact Information</h2>
	<span class="address"><?php echo config::CONTACT_ADDRESS; ?></span>
	<span class="phone">
		<a href="tel:<?php echo config::CONTACT_PHONE_1; ?>" title="Call <?php echo config::SITE_NAME; ?>">
			<?php echo config::CONTACT_PHONE_1; ?>
		</a>
	</span>
	<span class="email">
		<a href="mailto:<?php echo config::CONTACT_EMAIL; ?>" title="Email <?php echo config::SITE_NAME; ?>">
			<?php echo config::CONTACT_EMAIL; ?>
		</a>
	</span>
</div>
<?php
require_once '../resources/templates/tpl.footer.php';
?>