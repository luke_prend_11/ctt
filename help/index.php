<?php
require_once '../resources/inc.config.php';
require_once '../resources/templates/tpl.header.php';
?>
	<div class="help-item">
		<h2>About Us</h2>
		<p>With years of experience and fully qualified staff, <?php echo config::SITE_NAME; ?> offers professional and reliable close protection services.</p>
		<a href="<?php echo config::$baseUrl; ?>/help/about-us.php" title="About Us" class="btn blue">About Us</a>
	</div>
	<div class="help-item">
		<h2>Contact Us</h2>
		<p>If you have any questions about our services or training courses or you are requesting to hire our Security Services, please contact us and we will get back to you asap.</p>
		<a href="<?php echo config::$baseUrl; ?>/help/contact-us.php" title="Contact Us" class="btn blue">Contact Us</a>
	</div>
	<div class="help-item-last">
		<h2>Privacy Policy</h2>
		<p>View our websites Privacy Policy.</p>
		<a href="<?php echo config::$baseUrl; ?>/help/privacy.php" title="Privacy Policy" class="btn blue">Privacy Policy</a>
	</div>
<?php
require_once '../resources/templates/tpl.footer.php';
?>