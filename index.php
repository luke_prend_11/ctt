<?php
require_once 'resources/inc.config.php';
require_once 'resources/templates/tpl.header.php';
?>
			<p><?php echo config::SITE_NAME; ?> is in a unique position to offer the very best in Security, Health & Safety training, not only for the individual but for your entire work force and company. We have opened our own training facilities, conveniently situated in the heart of Birmingham City Centre. The company has surpassed the required standards and has been approved by Highfields. CTT can also provide bespoke training courses to suit your personal or business requirements, using their own qualified trainers operating within PTLLS/DTLLS.</p>
			<p>CTT are specialists in the training & development of individuals working in both the public & private sectors. These include the security industry, hospitality, health service, retail & licensed trades. Many of these industries are now under a legal requirement to provide training in line with Security Industry Authority (S.I.A.) requirements & other government legislation, Health & Safety Executive (H.S.E.).</p>
			<p>CTT will arrange cost-effective in-house Security plus Health & Safety training courses for organisations & small businesses requiring training at their own premises/site. You have the convenience of a qualified trainer, teaching all your employees at the same time, at a convenient city centre location.</p>
			<a href="<?php echo config::$baseUrl; ?>/help/contact-us.php" title="Contact Us" class="btn blue center">
				Contact Us
			</a>
			<br class="clearfloat" />
		</div>
	</div>
		
	<div class="very-light-grey">
		<div class="content">
			<img src="<?php echo config::$baseUrl; ?>/img/content/bird-of-prey.jpg" alt="Bird of Prey" class="float-left" />
			<h2 class="text-right">Close Protection and Counter Terrorism</h2>
			<p class="text-right">Close Tactical Training Ltd management team have well over 60 years combined experience in the Close Protection Industry to clients from entertainment, sport and music industries, Diplomats, Middle Eastern Royalty and Members of Parliament.</p>
			<a href="<?php echo config::$baseUrl; ?>/services/close-protection.php#h1" title="Close Protection and Counter Terrorism" class="btn grey float-right">
				Read More
			</a>
			<br class="clearfloat" />
		</div>
	</div>
	
	<div class="light-grey">
		<div class="content">
			<img src="<?php echo config::$baseUrl; ?>/img/content/construction.jpg" alt="Construction Site" class="float-right" />
			<h2>Manned Guarding</h2>
			<p>Empty premises, construction sites and new and used motor sales sites are a sitting target once businesses have closed for the day. Close Tactical Training Ltd can provide a twenty first century version of the “Night Watchman” to patrol and protect your sites.</p>
			<a href="<?php echo config::$baseUrl; ?>/services/manned-guarding.php#h1" title="Manned Guarding" class="btn grey float-left">
				Read More
			</a>
			<br class="clearfloat" />
		</div>
	</div>
		
	<div class="very-light-grey">
		<div class="content">
			<img src="<?php echo config::$baseUrl; ?>/img/content/security.jpg" alt="Security Guards" class="float-left" />
			<h2 class="text-right">Training</h2>
			<p class="text-right">At CTT, we offer courses in Close Protection, Door Supervision, Public Space Surveillance Operations, Physical Intervention, Conflict Management and Emergency First Aid.</p>
			<a href="<?php echo config::$baseUrl; ?>/training/" title="Training" class="btn grey float-right">
				Read More
			</a>
			<br class="clearfloat" />
			
<?php
require_once 'resources/templates/tpl.footer.php';
?>