- add google analytics script
- create favicon from logo graphic
- update meta content

- check if possible to remove anchors from url to prevent indexing
- redirect voucher-world subdomain to main domain - also do for sean moloney
- test redirect non www to www
- test contact form
- test in internet explorer

- send test link to james for checking
- make any changes
- make site live