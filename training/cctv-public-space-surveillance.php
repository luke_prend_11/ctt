<?php
require_once '../resources/inc.config.php';
require_once '../resources/templates/tpl.header.php';
?>

		<span class="btn blue float-left">4 Day Course: £185</span>
		<br class="clearfloat" />
		<p>Our team consists of highly experienced trainers who will help you to achieve quality training in CCTV Public Space Surveillance (PSS).</p>
		<p>Set over 4 days, this course is designed to help you get the basic job training and skills that meets the SIA requirements for licensing. The course enables operatives to apply for an SIA CCTV (PSS) license and will help you to develop and improve your knowledge within the sector. Once the course has been completed, you will have sound knowledge of not only the operational knowledge in regards to the CCTV systems, but fantastic understanding of the current legislation.</p>

		<p>The qualification consists of the following 3 units:</p>
		<ul class="list-points">
			<li><strong>Unit 1:</strong> Working in the Private Security Industry</li>
			<li><strong>Unit 2:</strong> Working as a CCTV Operator</li>
			<li><strong>Unit 3:</strong> Practical Operation of CCTV Equipment</li>
		</ul>

		<p>In order to obtain an SIA licence, you will have to provide your certificate of the course completion which we will provide for you. To pass the course and gain your qualification, you will have to attend the four day training course and pass 2 multiple choice exams, followed by a practical assessment.</p>
		
		<br />
		<h2>Core Learning and Qualifications for a CCTV (Public Space Surveillance) Licence</h2>
		
		<p>Core Learning for Common Security Industry Knowledge - 10 hours:</p>
		<ul class="list-points">
			<li><strong>Session 1:</strong> The Private Security Industry</li>
			<li><strong>Session 2:</strong> Communication Skills and Customer Care</li>
			<li><strong>Session 3:</strong> Awareness of the Law in the Private Security Industry</li>
			<li><strong>Session 4:</strong> Health and Safety for the Private Security Operative</li>
			<li><strong>Session 5:</strong> Fire Safety Awareness</li>
			<li><strong>Session 6:</strong> Emergency Procedures</li>
		</ul>
		
		<p>CCTV Operations (Public Space Surveillance) Specialist Module - 22 hours:</p>
		<ul class="list-points">
			<li><strong>Session 1:</strong> Roles and Responsibilities of the CCTV Operator and other CCTV Staff</li>
			<li><strong>Session 2:</strong> Codes of Practice, Operational Procedures and Guidelines</li>
			<li><strong>Session 3:</strong> CCTV Equipment and its Operation</li>
			<li><strong>Session 4:</strong> Control Room Communications and Access Control</li>
			<li><strong>Session 5:</strong> Legislation</li>
			<li><strong>Session 6:</strong> Dealing with Incidents</li>
			<li><strong>Session 7:</strong> CCTV Surveillance Techniques</li>
			<li><strong>Session 8:</strong> Emergency Procedures in the CCTV Control Room</li>
			<li><strong>Session 9:</strong> Health and Safety at Work in the CCTV Environment</li>
		</ul>
		
		<a href="<?php echo config::$baseUrl; ?>/help/contact-us.php" title="Contact Us" class="btn dark-grey float-left">
			Contact us for more information
		</a>
		
<?php
require_once '../resources/templates/tpl.footer.php';
?>