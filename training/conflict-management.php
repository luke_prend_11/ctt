<?php
require_once '../resources/inc.config.php';
require_once '../resources/templates/tpl.header.php';
?>

		<h2>Level 2</h2>
		<span class="btn blue float-left">1 Day Course: £75</span>
		<br class="clearfloat" />

		<p>This course is designed not just for individuals that are employed within the security sector, but for any one who is in contact with the public during their work based duties. Some individuals may feel they could possibly be faced with handling situations that are filled with aggression, confrontation and aggression. The Level 2 Award in Communication and Conflict management is designed to train you to combat these issues and to give you the knowledge and experience to feel comfortable if a situation should arise.</p>

		<p>During the one day course, we will assess individuals by a multiple choice exam and various practical sessions with observations. Once the course is completed, you will have gained and developed new and existing skills and grown in confidence when dealing with various problematic situations.</p>

		<p>Conflict management Level 2 comprises of the following two units:</p>

		<ul class="list-points">
			<li>Principles and Practices of Conflict Management Training</li>
			<li>Scenario-based Training in Conflict Management</li>
		</ul>
		
		<p>Conflict Management Module - 7 ½ hours contact time:</p>
		<ul class="list-points">
			<li><strong>Session 1:</strong> Introduction</li>
			<li><strong>Session 2:</strong> Preventing Conflict</li>
			<li><strong>Session 3:</strong> Managing Conflict</li>
			<li><strong>Session 4:</strong> Learning from Conflict</li>
		</ul>

		<br />
		<h2>Level 3</h2>
		<span class="btn blue float-left">3 Day Course: £175</span>
		<br class="clearfloat" />

		<p>This Level 3 qualification has been designed to teach trainers that want to provide conflict management training. Not only for people working within the security field, this course is designed for people from all employment backgrounds who have or feel the need and responsibility to train their employees on how to handle conflict whilst in a public facing role.</p>

		<p>The course will develop existing knowledge and see the applicants undergo practical sessions with observations as well as internally and externally assessed portfolios.</p>

		<p>Conflict management Level 3 comprises of the following units:</p>
		<ul class="list-points">
			<li>Principles and Practices of Conflict Management Training</li>
			<li>Scenario-based Learning and Training in Conflict Management</li>
			<li>Target Audience</li>
			<li>Security Industry or similar industries</li>
		</ul>
		
		<a href="<?php echo config::$baseUrl; ?>/help/contact-us.php" title="Contact Us" class="btn dark-grey float-left">
			Contact us for more information
		</a>
		
<?php
require_once '../resources/templates/tpl.footer.php';
?>