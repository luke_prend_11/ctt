<?php
require_once '../resources/inc.config.php';
require_once '../resources/templates/tpl.header.php';
?>

		<p>At CTT, we offer the following courses:</p>
		<ul class="list-none">
			<li>
				<a href="<?php echo config::$baseUrl; ?>/training/close-protection.php#h1" title="Close Protection Training">
					Level 3 Certificate in Close Protection
				</a>
			</li>
			<li>
				<a href="<?php echo config::$baseUrl; ?>/training/door-supervision.php#h1" title="Door Supervision Training">
					Level 2 Award in Door Supervision, plus Up Skills for Door Supervisors
				</a>
			</li>
			<li>
				<a href="<?php echo config::$baseUrl; ?>/training/cctv-public-space-surveillance.php#h1" title="CCTV Surveillance Training">
					Level 2 in CCTV (Public Space Surveillance) Operations
				</a>
			</li>
			<li>
				<a href="<?php echo config::$baseUrl; ?>/training/conflict-management.php#h1" title="Conflict Management Training">
					Conflict Management Level 2
				</a>
			</li>
			<li>
				<a href="<?php echo config::$baseUrl; ?>/training/physical-intervention.php#h1" title="Physical Intervention Training">
					Physical Intervention Level 2 and Level 3
				</a>
			</li>
			<li>
				<a href="<?php echo config::$baseUrl; ?>/training/first-aid.php#h1" title="First Aid Training">
					Emergency First Aid Level 2 and First Aid at Work
				</a>
			</li>
		</ul>
		
		<a href="<?php echo config::$baseUrl; ?>/help/contact-us.php" title="Contact Us" class="btn dark-grey float-left">
			Contact us for more information
		</a>
		
<?php
require_once '../resources/templates/tpl.footer.php';
?>