<?php
require_once '../resources/inc.config.php';
require_once '../resources/templates/tpl.header.php';
?>

		
		
		<div class="float-left page-wrap">
		<span class="btn blue float-left">28 Day Course: £3,000</span>
		<br class="clearfloat" />
		<p>The Close Protection course is run by our highly qualified and knowledgeable trainers who have over 100 years of CP experience combined throughout various areas within the security industry. The Level 3 Award in Close Protection Operations surpasses the necessary criteria successful candidates will need to go through so they are able to move forward and apply for an SIA licence. Previous experience, employment or knowledge within the security industry is not a requirement we ask you to have when applying to join us on the CP course, just the desire to learn and progress. CTT is able to offer training in Close Protection at our offices in Snow Hill, conveniently located in Birmingham City Centre.</p>
		
		<p><strong>The Close protection course is priced at £3,000 with a deposit of £500 to secure your place.</strong></p>
		
		<p>There are a number of units you must complete in order to achieve your Level 3 award in Close Protection Operations over a 17 day period. The units and their core details are as follows:</p>
		<ul class="list-points">
			<li><strong>Skills and Teamwork in Close Protection:</strong> Essential for the CPO and the knowledge and skills required are developed over the 17 days and this is achieved through a combination of lectures, practical exercises and scenarios based on the common duties of a modern day CPO. The training peaks with an intensive final exercise that combines all the skills and knowledge obtained throughout the duration.</li>
			<li><strong>Legislation, Communication and Conflict Management Skills:</strong> The role of a CPO requires individuals to be fully conversant with the law and to understand the relationships between the CP industry and official agencies. Again this is achieved by classroom discussions, videos and practical scenarios.</li>
			<li><strong>Risk Assessment, Operational Planning and Safe Route Selection:</strong> It is important to identify and quantify the risks when working as a CPO and good planning and preparation is an essential part of the role. Exercises and classroom activities incorporate a large part of this module where delegates will actually plan and prepare for an operation leading to the final exercise.</li>
			<li><strong>Surveillance, Reconnaissance, Incidents & Dilemmas:</strong> This module will introduce you to the concept of surveillance and counter surveillance and is key to protecting clients as most pre-meditated attacks include some form of surveillance. Preventative and proactive measures to detect surveillance are taught and related to real life incidents. There must also be procedures for when things don't go as planned and the ability to think on your feet will be developed during the course.</li>
			<li><strong>Techniques for Close Protection Operatives:</strong> Most of the physical skills required by a CPO are covered in this final module such as vehicle drills and convoy driving through to foot techniques and searching procedures will be practiced to hone these essential skill.</li>
			<li><strong>Anti-Ambush Training:</strong> Designed to be as realistic as possible the day spent on this training will demonstrate the difficulties a realism of extracting a principal under attack.</li>
		</ul>
		
		<br />	
		<h2>Close Protection Course Criteria</h2>
		<ul class="list-points">
			<li>Understand the legislation that is relevant to people working in the close protection industry</li>
			<li>Understand the roles and responsibilities of the close protection operative</li>
			<li>Know the importance of threat assessment and risk management</li>
			<li>Know surveillance techniques</li>
			<li>Understand venue security operations</li>
			<li>Know how to design and demonstrate operational planning</li>
			<li>Know the importance of interpersonal skills</li>
			<li>Know the importance of teamwork</li>
			<li>Know the importance of reconnaissance</li>
			<li>Know how to conduct close protection foot drills</li>
			<li>Know the importance of planning and selecting routes</li>
			<li>Know vehicle movement tactics and operations</li>
			<li>Know the search techniques and procedures for close protection operations</li>
			<li>Know how to apply conflict management techniques while providing close protection</li>
		</ul>
		
		<br />	
		<h2>Working as a Close Protection Operative</h2>
		<p>This course contains a lot of different information. For example you could be protecting a Footballer, Singer, Politician or High Profile Businessperson. The variations in themselves can create many different types of security risks you will have to account for. See the list below for further information of the role of a close protection operative.</p>
		
		<h3 class="toggle dark-grey">+ Close Protection Operative Skill Set</h3>
		<div class="toggle">
			<ul class="list-none">
				<li><strong>Threat and Risk Assessment</strong> – A bodyguard must maintain 100% concentration when working with the Principal at all times.  When moving from one place to another there are countless potential risks, which the Close Protection Officer must be ready for, being able to react to each situation as they arise.</li>
				<li><strong>Surveillance Awareness</strong> – A Close Protection Officer may only have moments in which to make a move or identify a potential route or target. Identifying when the time is right to move the Principal or Protect from chasing Photographers and Fans, ensuring a safe route and environment at all times is vital.</li>
				<li><strong>Operational Planning</strong> – A Close Protection Officer must be able to plan the daily schedule when guarding their Principal, most days and weeks are usually scheduled in advance. This enables you to plan with precise precision how they can travel to each engagement or appointment with only minimal disruption.</li>
				<li><strong>Law and Legislation</strong> – A Close Protection Officer must be aware of the law and how to operate efficiently and effectively within it. Not putting yourself or your Principal at risk from prosecution is vital as Business and Personal reputations can be ruined, leading to loss of contracts or unemployment.</li>
				<li><strong>Interpersonal Skills</strong> – A Close Protection Officer must be able to use effective communications skills on all levels; you may be dealing with angry and upset clients including business colleagues and fans who feel they have been hard done by. Press and radio interviewers wanting to get a last minute interview or final picture and won’t take no for an answer. Pestering fans or heckler can aggravate or upset the Principal so it’s worth learning how to defuse situations sensibly and quickly.</li>
				<li><strong>Close Protection Teamwork</strong> – The role of protecting an Individual or business becomes vastly more practical when two or more Close Protection Officers work together. It is vital to understand how to be a team player, ensuring individually as well as collectively the task is completed using the right operatives to ensure total peace of mind to the client.</li>
				<li><strong>Reconnaissance</strong> – A Close Protection Officer must have advanced knowledge of alternative routes, being able to avoid angry crowds and paparazzi if they turn up. Selection and understanding of route selection is important, to know the best route will allow you to know who and places including areas to avoid. This will allow the Principal to travel safely ensuring their personal safety from unwanted attention and harassment.</li>
				<li><strong>Close Protection Foot Techniques</strong> – A Close Protection Officer must be able to walk any distance from place to place with a Principal, allowing private conversations or calls to take place, but being close enough to provide cover or deflection from crowds or pedestrians. On the street is where the Principal is most venerable, you should know the tricks of the trade.</li>
				<li><strong>Route Selection</strong> – A Close Protection Officer has to pick the right route or alternative route if so required. Traffic Jams and road works will delay the Principal, traveling in the rush hour could attract unwanted attention and bring unwanted harassment. Mapping and planning are a vital part of everyday life depending on the profile of the Client.</li>
				<li><strong>Close Protection Vehicle Techniques</strong> – A Close Protection Officer must know how to get the Principal into the car, out of the car, and how to drive past crowds without putting your Client at risk. Embus and Debus is important, what side do they enter or exit the vehicle, is there fuel and food in the Vehicle for the journey ahead, are there any special requirements, is the Vehicle road worthy!</li>
				<li><strong>Search Procedures</strong> – A Close Protection Officer must know how to search a venue, vehicles and persons, looking for IEDs, Cameras, etc, including any hazards that may cause risk of harm to your clients.</li>
				<li><strong>Incidents and Dilemmas</strong> – Crisis Management.</li>
				<li><strong>TACT</strong> - Think: SAFETY FIRST; Assess: THE SITUATION; Consider: YOU’RE OPTIONS; Take: ACTION</li>
				<li><strong>Venue-Based Security</strong> – A Close Protection Officer must ensure the venue is safe to enter and operate in, making sure that no-one can hide an IED or themselves  somewhere in the venue and spring a nasty surprise/attack. Authorised visitors makes the job easier, signing in and out procedures help to identify with the correct ID.</li>
				<li><strong>Communication and Conflict Management Skills</strong> – Close Protection Officers on the course will engage in various scenarios relating to Communication and Conflict Management largely self-explanatory.</li>
			</ul>
		</div>
		
		<br />
		<p>All of our CTT courses are full on, practical and action packed, ctt provide many extra-curricular activities that other security training providers neglect.</p>
		<p>CCT Close Protection Course is an intensive period of instruction that will give the candidates a solid base to build his or her Close Protection expertise. The Security Industry Authority (SIA) will issue operating licenses to successful candidates upon application.</p>
		<p>Some of the exercises during the course will finish late, so you should prepare yourself for long days - just like real operations.</p>
		
		<br />	
		<h2>Requirements, Examination and License</h2>
		<ul class="list-points">
			<li><strong>Minimum Requirements:</strong> Candidates must be 18 or above and should hold a full driving licence although not mandatory. A minimum First Aid at Work qualification is required for licensing under SIA regulations. We can incorporate the relevant First Aid Training for an additional fee. Candidates must be of reasonable health and fitness levels due to the nature of the course.</li>
			<li><strong>Examination:</strong> The examination is by way of multiple choice question paper and successful candidates will achieve a Level 3 Award in Close Protection Operations and be eligible to apply for an SIA licence.</li>
			<li><strong>SIA Licence:</strong> Upon successful completion of the course and passing the required examination, you will be eligible to apply for an SIA licence. We will assist you where needed in applying for your licence and avoid any unnecessary delays.</li>
		</ul>
		
		<a href="<?php echo config::$baseUrl; ?>/help/contact-us.php" title="Contact Us" class="btn dark-grey float-left">
			Contact us for more information
		</a>
		</div>
		<div class="float-right blue sidebar">
			<h3>About Our CPOs</h3>
			<p>CTT Close Protection Officers are experienced in V.I.P / Close Protection Services; having undergone rigorous Close Protection training, including close quarter unarmed combat & physical intervention and disengagement techniques. CTTClose Protection Operatives are fully qualified in First Aid, Response Trauma Medical Care.</p>

			<p>CTT go above and beyond the Security Industry Authority (S.I.A.) criteria ensuring our Close Protection Services are provided by Officers/Bodyguards equipped with all information & up to date electronic technology. CTT can provide a dedicated security contract manager if required for the duration of the Security Contract, to deal with all security issues as they arise. This also ensures all Security Operatives are kept up to speed with the required task ahead.</p>

			<p>CTT has worked with High Profile Personnel & Blue chip companies who feel they are at risk from unwanted harassment & attention. They may also be a target for jealousy & crimes against their possessions, as their lives/business are public knowledge together with all their assets.</p>

			<p>A discreet security risk analysis is carried out resulting in detailed strategic planning which will provide bespoke security measures allowing the daily routine of your business & family life to continue with only minimal disruption.</p>
		</div>
		
<?php
require_once '../resources/templates/tpl.footer.php';
?>