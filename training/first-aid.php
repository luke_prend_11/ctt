<?php
require_once '../resources/inc.config.php';
require_once '../resources/templates/tpl.header.php';
?>

		<span class="btn blue float-left margin-right">1 Day Course: £110</span>
		<span class="btn blue float-left">3 Day Course: £250</span>
		<br class="clearfloat" />
		
		<p>The Health and Safety Executive (HSE) Approved 3-Day First Aid at Work Training Course provides training for qualification as a First Aider within the workplace under the guidance of the Health & Safety Regulations 1981.</p>
		
		<a href="<?php echo config::$baseUrl; ?>/help/contact-us.php" title="Contact Us" class="btn dark-grey float-left">
			Contact us for more information
		</a>
		
<?php
require_once '../resources/templates/tpl.footer.php';
?>