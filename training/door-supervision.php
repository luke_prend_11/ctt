<?php
require_once '../resources/inc.config.php';
require_once '../resources/templates/tpl.header.php';
?>

		<span class="btn blue float-left">4 Day Course: £150</span>
		<br class="clearfloat" />
		<p>In 2003, the SIA Licensing was introduced. This now means that in order to work in the security industry as a Door Supervisor you must attend and pass your Level 2 qualification in Door Supervision. Within our team of highly skilled and trained individuals who are continually operational in this sector they will endeavour to help and to guide you to gain the necessary understanding in this industry and mentor you throughout the licensing process.</p>
		<p>There are various steps you need to take In order to obtain an SIA licence. To do this you will need to show that you are trained to the appropriate level. To achieve this particular qualification, you must pass 4 multiple choice exams followed by a practical assessment of physical intervention. The SIA Door Supervisor Training Qualification consists of 30 hours contact training time over 4 days which our trainers will be there to mentor you on.</p>
		
		<br />
		<h2>Course Content</h2>
		<p>Core Learning for Common Security Industry Knowledge - 5 hours contact time.</p>
		<ul class="list-none">
			<li><strong>Session 1:</strong> The Private Security Industry</li>
			<li><strong>Session 2:</strong> Communication Skills and Customer Care</li>
			<li><strong>Session 3:</strong> Awareness of the Law in the Private Security Industry</li>
			<li><strong>Session 4:</strong> Health and Safety for the Private Security Operative</li>
			<li><strong>Session 5:</strong> Fire Safety Awareness</li>
			<li><strong>Session 6:</strong> Emergency Procedures</li>
		</ul>
		<br />
		<p>Door Supervisor Specialist Module - 10 hours contact time.</p>
		<ul class="list-none">
			<li><strong>Session 1:</strong> Introduction</li>
			<li><strong>Session 2:</strong> The Door Supervisor and the Law</li>
			<li><strong>Session 3:</strong> Arrest</li>
			<li><strong>Session 4:</strong> Licensing Law</li>
			<li><strong>Session 5:</strong> Searching</li>
			<li><strong>Session 6:</strong> Drugs</li>
			<li><strong>Session 7:</strong> Recording incidents and crime scene preservation</li>
			<li><strong>Session 8:</strong> Emergency Procedures</li>
		</ul>
		
		<br />
		<h2>Level 2 Award in Up-skilling Door Supervisors</h2>
		<span class="btn blue float-left">1 Day Course: £75</span>
		<br class="clearfloat" />
		<p>Any current SIA Door Supervisor Licence holders now have to undergo additional training. From February 2013, this course became mandatory. If you are renewing your licence or due to renew you must then complete the up-skilling course.</p>
		<p>The new qualification comprises of the following 2 sectors:</p>
		<ul class="list-points">
			<li>Physical Intervention Skills for the Private Security Industry - assessed by a multiple choice exam and a practical assessment.</li>
			<li>Safety Awareness for Door Supervisors will be assessed by a multiple choice exam only.</li>
		</ul>

		<a href="<?php echo config::$baseUrl; ?>/help/contact-us.php" title="Contact Us" class="btn dark-grey float-left">
			Contact us for more information
		</a>
		
<?php
require_once '../resources/templates/tpl.footer.php';
?>