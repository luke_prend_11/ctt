<?php
require_once '../resources/inc.config.php';
require_once '../resources/templates/tpl.header.php';
?>

		<span class="btn blue float-left">1 Day Course: £75</span>
		<br class="clearfloat" />
		<br />
		<h2>Physical Intervention Skills</h2>
		<p>The Physical Intervention Skills unit for the Private Security Industry (as per the new full qualification) will be assessed by multi choice examination and practical assessment.</p>
		
		<br />
		<h2>Safety Awareness for Door Supervisors</h2>
		<p>The new Safety Awareness for Door Supervisors unit has been developed to cover the following subjects within the security sector:</p>
		
		<ul class="list-points">
			<li>Dealing with young people</li>
			<li>First Aid Awareness</li>
			<li>Counter Terrorism</li>
		</ul>
		<p>There will be a multi choice examination for the module; Safety Awareness for Door Supervisors.</p>
		
		<a href="<?php echo config::$baseUrl; ?>/help/contact-us.php" title="Contact Us" class="btn dark-grey float-left">
			Contact us for more information
		</a>
		
<?php
require_once '../resources/templates/tpl.footer.php';
?>