<?php
require_once 'resources/inc.config.php';
require_once 'resources/templates/tpl.header.php';
?>
	<ul class="list-none level-1">
		<li><a href="<?php echo config::$baseUrl; ?>" title="Home">Home Page</a></li>
		<li><a href="<?php echo config::$baseUrl; ?>/services/" title="Our Services">Our Services</a></li>
		<li><a href="<?php echo config::$baseUrl; ?>/help/" title="Help">Help</a>
			<ul class="list-none">
				<li><a href="<?php echo config::$baseUrl; ?>/help/about-us.php" title="About Us">About Us</a></li>
				<li><a href="<?php echo config::$baseUrl; ?>/help/contact-us.php" title="Contact Us">Contact Us</a></li>
				<li><a href="<?php echo config::$baseUrl; ?>/help/privacy-policy.php" title="Privacy Policy">Privacy Policy</a></li>
			</ul>
		</li>	
	</ul>
<?php
require_once 'resources/templates/tpl.footer.php';
?>