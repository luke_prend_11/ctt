<?php
require_once '../resources/inc.config.php';
require_once '../resources/templates/tpl.header.php';
?>

		<p>At CTT, we offer the following services:</p>
		<ul class="list-none">
			<li>
				<a href="<?php echo config::$baseUrl; ?>/services/close-protection.php#h1" title="Close Protection Services">
					Close Protection
				</a>
			</li>
			<li>
				<a href="<?php echo config::$baseUrl; ?>/services/retail-security.php#h1" title="Retail Security Services">
					Retail Security
				</a>
			</li>
			<li>
				<a href="<?php echo config::$baseUrl; ?>/services/manned-guarding.php#h1" title="Manned Guarding Services">
					Manned Guarding
				</a>
			</li>
			<li>
				<a href="<?php echo config::$baseUrl; ?>/services/security-guards.php#h1" title="Security Guard Services">
					Provision of Security Guards
				</a>
			</li>
		</ul>
			
		<a href="<?php echo config::$baseUrl; ?>/help/contact-us.php" title="Contact Us" class="btn dark-grey float-left">
			Contact us for more information
		</a>
		
<?php
require_once '../resources/templates/tpl.footer.php';
?>