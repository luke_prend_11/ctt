<?php
require_once '../resources/inc.config.php';
require_once '../resources/templates/tpl.header.php';
?>

		<p>Our Security Services Management team have over 30 years experience in all areas of the private security industry. We provide bespoke security services to clients across the country, from single site to multi-site operations. Our services range from Retail Security and Key Holding / Emergency Response to Close Protection and Door Supervisors.</p>
		<p>All our security guards and staff are fully trained, licensed in accordance with SIA (Security Industry Authority) Legislation and screened to BS7858 (5 year work or back to school checkable history & CRB Cleared).</p>
		<p>At CTT we operate a transparent business model, for that reason we install real time proof of presence systems that enable both us and our security service customers to track our security officer's patrol / location on site 24/7 365 days a year. Whatever your Security Requirements we can facilitate your needs:</p>
		<ul class="list-points">
			<li>Building Site Security Services</li>
			<li>Door Supervisors</li>
			<li>Close Protection</li>
			<li>Emergency Response</li>
			<li>Event Stewards & Guards</li>
			<li>Gatehouse Security Guards</li>
			<li>Loss Prevention</li>
			<li>Mail Room Security Guards</li>
			<li>Mobile Patrols & Security Services</li>
			<li>Reception / Front of House</li>
			<li>Retail Security Guards</li>
			<li>Security Dogs & Specialist Dog Units</li>
			<li>Warehouse</li>
			<li>Security Guards</li>
		</ul>
		
		<a href="<?php echo config::$baseUrl; ?>/help/contact-us.php" title="Contact Us" class="btn dark-grey float-left">
			Contact us for more information
		</a>
		
<?php
require_once '../resources/templates/tpl.footer.php';
?>