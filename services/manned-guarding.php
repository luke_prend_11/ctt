<?php
require_once '../resources/inc.config.php';
require_once '../resources/templates/tpl.header.php';
?>

		<p>Empty premises, construction sites and new and used motor sales sites are a sitting target once businesses have closed for the day. Close Tactical Training Ltd can provide a twenty first century version of the “Night Watchman” to patrol and protect your sites. He can be equipped with a CCTV camera integrated into his uniform to enable, not only incidents to be recorded, but also to demonstrate to our clients their accountability during contracted hours.</p>
		<p>All our staff are qualified SIA licensed security officers with the safety of persons, premises and property their paramount concern.</p>
		<p>These traditional Security Guards are backed up by both our 24 hour Central Control Room, which enable our operators to be kept informed as incidents develop and co-ordinate necessary responses, and the use of Mobile Fast Response Units.</p>
		<p>These Mobile Fast Response Units have a variety of uses:</p>

		<ul class="list-points">
			<li>Random checks on manned guard sites.</li>
			<li>React to reported incidents and to assist manned guard.</li>
			<li>React to alarm activations and determine causes.</li>
			<li>Will attend with client’s staff to assess situation giving them peace of mind when entering suspect buildings.</li>
			<li>In contact with Control Room for Fast Response to incidents.</li>
		</ul>
		
		<a href="<?php echo config::$baseUrl; ?>/help/contact-us.php" title="Contact Us" class="btn dark-grey float-left">
			Contact us for more information
		</a>
		
<?php
require_once '../resources/templates/tpl.footer.php';
?>