<?php
require_once '../resources/inc.config.php';
require_once '../resources/templates/tpl.header.php';
?>

		<p>Although ten years ago retail shrinkage (stock loss caused by crime etc. as a percentage of sales), was as high as 1.77%. In 2012/13 it had fallen to 1.32% which equates to the following:</p>
	
		<table>
			<tr class="table-row1">
				<td>Customer fraud and theft</td>
				<td>£2206 M</td>
			</tr>
			<tr class="table-row0">
				<td>Employee fraud and theft</td>
				<td>£1681 M</td>
			</tr>
			<tr class="table-row1">
				<td>Others (Card fraud, burglary, etc.)</td>
				<td>£812 M</td>
			</tr>
			<tr class="table-row0 total">
				<td><strong>Total losses in the twelve months 2012/2013</strong></td>
				<td><strong>£4699 M</strong> (6.8% higher than 2011/12)</td>
			</tr>
		</table>
		<span class="caption">Source: Centre for Retail Research, Newark</span>

		<p>With the economic climate as it is, crime will only increase as people are trying to make ends meet! Close Tactical Training Ltd have been providing retail security solutions to businesses nationwide for many years and our Retail Security Services Team have over 45 years combined experience in the retail security services industry.</p>
		<p>We initially carry out a security audit and will then suggest improvements which should reduce losses. We then brief our Retail Security Team, together with the clients own security staff, on the potential weaknesses and will implement security improvements and instructions to help to continuously cut losses.</p>
		
		<a href="<?php echo config::$baseUrl; ?>/help/contact-us.php" title="Contact Us" class="btn dark-grey float-left">
			Contact us for more information
		</a>
		
<?php
require_once '../resources/templates/tpl.footer.php';
?>