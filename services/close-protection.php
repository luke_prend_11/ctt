<?php
require_once '../resources/inc.config.php';
require_once '../resources/templates/tpl.header.php';
?>

		<p>Close Tactical Training Ltd management team have well over 60 years combined experience in the Close Protection Industry to clients from entertainment, sport and music industries, Diplomats, Middle Eastern Royalty and Members of Parliament.</p>
		<p>Close Tactical Training Ltd have supplied Close Protection to clients throughout the UK, USA, Europe, Saudi Arabia and Africa.</p>
		<p>In the current world it is not uncommon for our Close Protection Officers to be utilised by clients from corporate or business backgrounds who understand the need for discreet and professional personal protection.</p>
		<a href="<?php echo config::$baseUrl; ?>/help/contact-us.php" title="Contact Us" class="btn dark-grey float-left">
			Contact us for more information
		</a>
		
<?php
require_once '../resources/templates/tpl.footer.php';
?>